<?php

namespace Drupal\Tests\sandpit\Functional;

/**
 * Tests.
 *
 * @group sandpit
 */
class SandpitBasicTest extends SandpitBrowserTestBase {

  /**
   * Tests basic scheduling of content.
   */
  public function test1() {
    $this->drupalLogin($this->adminUser);
    $this->assert(TRUE, 'The test did not run.');
  }

}
